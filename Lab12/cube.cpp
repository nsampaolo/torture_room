//FileName:		cube.cpp
//Programmer:	Dan Cliburn
//Date:			9/17/2014
//Purpose:		To define the model that we want to render in our OpenGL program.

#include <glew.h>  //glew.h is supposed to be included before gl.h.  To be safe, you can just include glew.h instead
#include "cube.h"
#include <iostream>
#include <string>
using namespace std;

Cube::Cube()
{
	VAO = 0;
	initialized = false;
}

//init() does all of the OpenGL initialization for your model(s)
bool Cube::init()
{
	//glGenVertexArrays(1, &VAO);  //Create one vertex array object
	//glBindVertexArray(VAO);

	GLfloat vertices[4][3];
	//front face
	vertices[0][0] = -0.9; //x value
	vertices[0][1] = 0.9; //y value
	vertices[0][2] = 0.9; //z value
	vertices[1][0] = -0.9; //x value
	vertices[1][1] = -0.9; //y value
	vertices[1][2] = 0.9; //z value
	vertices[2][0] = 0.9; //x value
	vertices[2][1] = 0.9; //y value
	vertices[2][2] = 0.9; //z value
	vertices[3][0] = 0.9; //x value
	vertices[3][1] = -0.9; //y value
	vertices[3][2] = 0.9; //z value

	

	//TODO: put correct values into v
	sides[0].init("pic/Hbrick01.bmp", vertices);



	//two more vertices for right face
	vertices[0][0] = 0.9; //x value
	vertices[0][1] = 0.9; //y value
	vertices[0][2] = 0.9; //z value
	vertices[1][0] = 0.9; //x value
	vertices[1][1] = -0.9; //y value
	vertices[1][2] = 0.9; //z value
	vertices[2][0] = 0.9; //x value
	vertices[2][1] = 0.9; //y value
	vertices[2][2] = -0.9; //z value
	vertices[3][0] = 0.9; //x value
	vertices[3][1] = -0.9; //y value
	vertices[3][2] = -0.9; //z value


	sides[1].init("pic/SP_DUDE8.bmp", vertices);


	//two more vertices for back face
	vertices[0][0] = 0.9; //x value
	vertices[0][1] = 0.9; //y value
	vertices[0][2] = -0.9; //z value
	vertices[1][0] = 0.9; //x value
	vertices[1][1] = -0.9; //y value
	vertices[1][2] = -0.9; //z value
	vertices[2][0] = -0.9; //x value
	vertices[2][1] = 0.9; //y value
	vertices[2][2] = -0.9; //z value
	vertices[3][0] = -0.9; //x value
	vertices[3][1] = -0.9; //y value
	vertices[3][2] = -0.9; //z value

	sides[2].init("pic/Hbrick01.bmp", vertices);

	//Repeat the first two vertices so that we can draw with a GL_TRIANGLE_STRIP
	vertices[0][0] = -0.9; //x value
	vertices[0][1] = 0.9; //y value
	vertices[0][2] = -0.9; //z value
	vertices[1][0] = -0.9; //x value
	vertices[1][1] = -0.9; //y value
	vertices[1][2] = -0.9; //z value
	vertices[2][0] = -0.9; //x value
	vertices[2][1] = 0.9; //y value
	vertices[2][2] = 0.9; //z value
	vertices[3][0] = -0.9; //x value
	vertices[3][1] = -0.9; //y value
	vertices[3][2] = 0.9; //z value

	sides[3].init("pic/Hbrick01.bmp", vertices);

	//Four vertices for the top
	vertices[0][0] = -0.9; //x value
	vertices[0][1] = 0.9; //y value
	vertices[0][2] = -0.9; //z value
	vertices[1][0] = -0.9; //x value
	vertices[1][1] = 0.9; //y value
	vertices[1][2] = 0.9; //z value
	vertices[2][0] = 0.9; //x value
	vertices[2][1] = 0.9; //y value
	vertices[2][2] = -0.9; //z value
	vertices[3][0] = 0.9; //x value
	vertices[3][1] = 0.9; //y value
	vertices[3][2] = 0.9; //z value

	sides[4].init("pic/concrete.bmp", vertices);

	//Four vertices for the top
	vertices[0][0] = -0.9; //x value
	vertices[0][1] = -0.9; //y value
	vertices[0][2] = 0.9; //z value
	vertices[1][0] = -0.9; //x value
	vertices[1][1] = -0.9; //y value
	vertices[1][2] = -0.9; //z value
	vertices[2][0] = 0.9; //x value
	vertices[2][1] = -0.9; //y value
	vertices[2][2] = 0.9; //z value
	vertices[3][0] = 0.9; //x value
	vertices[3][1] = -0.9; //y value
	vertices[3][2] = -0.9; //z value

	sides[5].init("pic/concrete.bmp", vertices);

	/*vertices[0][0] = -0.9; //x value
	vertices[0][1] = 0.9; //y value
	vertices[0][2] = 0.9; //z value
	vertices[1][0] = -0.9; //x value
	vertices[1][1] = -0.9; //y value
	vertices[1][2] = 0.9; //z value
	vertices[2][0] = 0.9; //x value
	vertices[2][1] = 0.9; //y value
	vertices[2][2] = 0.9; //z value
	vertices[3][0] = 0.9; //x value
	vertices[3][1] = -0.9; //y value
	vertices[3][2] = 0.9; //z value*/



	//TODO: put correct values into v
//	sides[6].init("pic/hell_knight.bmp", vertices);

	/*
	glGenBuffers(1, &Buffer); //Create a buffer objects for vertex positions
	glBindBuffer(GL_ARRAY_BUFFER, Buffer);  //Buffers[0] will be the position for each vertex
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);  //Do the shader plumbing here for this buffer
	glEnableVertexAttribArray(0);  //position is attribute location 0
	*/
	//NOTE: Since many vertices will share the values for color and surface normal, 
	//we'll set those with static vertex attributes in the draw() method.

	initialized = true;
	return true;  //Everything got initialized
}

//draw() explains how to render your model
void Cube::draw()
{
	if (initialized == false)
	{
		cout << "ERROR: Cannot render a Cube object before it has been initialized." << endl;
		return;
	}
	//glBindVertexArray(VAO);

	//First set the shininess in attribute location 3
	glVertexAttrib1f(3, 2.0);	//All sides will have the same "shininess".  This might seem
								//counterintuitive, but the smaller this number the more
								//noticable the specular highlights will be.
	//Make front face red
	glVertexAttrib4f(1, 1.0, 0.0, 0.0, 1.0);  //color is attribute location 1
	glVertexAttrib3f(2, 0.0, 0.0, 1.0);  //normal is attribute location 2
	//glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	sides[0].draw();


	//Make right face green
	glVertexAttrib4f(1, 1.0, 1.0, 1.0, 1.0);
	glVertexAttrib3f(2, 1.0, 0.0, 0.0);  //normal is attribute location 2
	//glDrawArrays(GL_TRIANGLE_STRIP, 2, 4);
	sides[1].draw();

	//Make back face blue
	glVertexAttrib4f(1, 0.0, 0.0, 1.0, 1.0);
	glVertexAttrib3f(2, 0.0, 0.0, -1.0);  //normal is attribute location 2
	//glDrawArrays(GL_TRIANGLE_STRIP, 4, 4);
	sides[2].draw();

	//Make left face yellow
	glVertexAttrib4f(1, 1.0, 1.0, 0.0, 1.0);
	glVertexAttrib3f(2, -1.0, 0.0, 0.0);  //normal is attribute location 2
	//glDrawArrays(GL_TRIANGLE_STRIP, 6, 4);
	sides[3].draw();

	//Make top face cyan
	glVertexAttrib4f(1, 1.0, 1.0, 1.0, 1.0);
	glVertexAttrib3f(2, 0.0, 1.0, 0.0);  //normal is attribute location 2
	//glDrawArrays(GL_TRIANGLE_STRIP, 10, 4);
	sides[4].draw();

	//Make bottom face pink
	//glVertexAttrib4f(1, 1.0, 0.0, 1.0, 1.0);
	glVertexAttrib3f(2, 0.0, -1.0, 0.0);  //normal is attribute location 2
	//glDrawArrays(GL_TRIANGLE_STRIP, 14, 4);
	sides[5].draw();
}
/*
void Cube::draw1()
{
	if (initialized == false)
	{
		cout << "ERROR: Cannot render a Cube object before it has been initialized." << endl;
		return;
	}
	//glBindVertexArray(VAO);

	//First set the shininess in attribute location 3
	glVertexAttrib1f(3, 2.0);	//All sides will have the same "shininess".  This might seem
	//counterintuitive, but the smaller this number the more
	//noticable the specular highlights will be.
	//Make front face red
	glVertexAttrib4f(1, 1.0, 0.0, 0.0, 1.0);  //color is attribute location 1
	glVertexAttrib3f(2, 0.0, 0.0, 1.0);  //normal is attribute location 2
	//glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	sides[6].draw();
}*/