//FileName:		rectangle.h
//Programmer:	Dan Cliburn
//Date:			10/10/2014
//Purpose:		This file defines the header for a rectangle class.

#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <glew.h>  //glew.h is supposed to be included before gl.h.  To be safe, you can just include glew.h instead
#include <gl/GLU.h>
#include <string>
#include "quad.h"
using namespace std;

class Rectangle : public Quad
{
public:
	virtual void defineVertexPositions();
};

#endif