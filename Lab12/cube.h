//FileName:		cube.h
//Programmer:	Dan Cliburn
//Date:			9/17/2014
//Purpose:		This file defines the header for a cube that is centered at the origin

#ifndef CUBE_H
#define CUBE_H

#include <glew.h>  //glew.h is supposed to be included before gl.h.  To be safe, you can just include glew.h instead
#include "texmodel.h"

class Cube
{
private:
	bool initialized;

	//GLfloat vertices[18][3];  //each vertex has an x, y, and z value
	GLfloat color[4];
	GLfloat normal[3];
	GLuint VAO;
	GLuint Buffer;  //We'll need a buffer for the vertex positions
	TexModel sides[6];


public:
	Cube();

	bool init();
	void draw();
	//void draw1();
};

#endif