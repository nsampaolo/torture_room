//FileName:		main.cpp
//Programmer:	Dan Cliburn
//Date:			10/1/2014
//Purpose:		This file defines the main() function for Lab 12.
//Movement is controller with wasd and view direction is changed with the mouse.
//The program ends when the user presses the <Esc> key

#include "viewcontroller.h"
#include <stdlib.h>

int main(int argc, char *argv[])  //main() must take these parameters when using SDL
{
	Viewcontroller vc;

	vc.run();

	system("pause");
	return 0;
}